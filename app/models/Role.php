<?php

/**
 * Description of Role
 *
 * @author Juan
 */
class Role extends Eloquent {
    
    /**
     * Obtiene los usuarios asignados por medio de la relación muchos a muchos
     * @return type
     */
    public function users()
    {
        return $this->belongsToMany('User');
    }
}

?>
