@extends('users.login.layout')
@section('title')
Regístrate en Sinexiones
@endsection
@section('header')
Únete a Sinexiones
@endsection
@section('action')
login
@endsection
@section('inputs')
<input class="entrada_registro" name="email" id="email" type="text" value="E-mail"/>
<div class="mensaje"></div>
<input class="entrada_registro" name="password" id="password" type="password" value=""/>
<div class="mensaje"></div>
<input class="entrada_registro" type="submit" value="Enviar" id="enviar"/>
@endsection