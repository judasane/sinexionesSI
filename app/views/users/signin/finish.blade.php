@extends('users.signin.layout')
@section('title')
Finaliza tu registro
@endsection
@section('header')

    @if ($gender=='male')
        Bienvenido {{{$name}}}
    @else
        Bienvenida {{{$name}}}
    @endif
@endsection
@section('action')
finish-signin
@endsection
@section('inputs')
<div class="radio">
<label for="password">Contraseña</label>
<input name="password" id="password" type="password" value=""/>
<div class="mensaje"></div>
</div>
<input type="hidden" value="{{{$id}}}" name="id"/>
<input class="entrada_registro" type="submit" value="Enviar" id="enviar"/>
@endsection