@extends('layout')
@section('head')
@parent
<title>Regístrate en Sinexiones</title>
<?php
echo HTML::script("js/jquery-1.8.0.min.js");
echo HTML::script("js/modeloJquery.js");
echo HTML::script("js/controladorJQuery.js");
echo HTML::style("header_principal.css");
echo HTML::style("layout.css");
echo HTML::style("registro.css");
?>
@stop
@section('header_principal')
<div id="logo">
    <img src="../img/logo1000.png" height="70" width="70" alt="logo">sinexiones
</div>
@parent
@stop
@section('contenedor_cuerpo')
<div id="contenedor_registro">
    <h1>Formulario de Registro</h1>
    <form action="signin" method="POST" id="form_registro">
        <fieldset form="form_registro">
            <legend>Ingresa tus datos básicos para formar parte de Sinexiones</legend>
            <label class="label_caja" for="name">Ingresa tu nombre:</label>
            <input class="caja" id="name" name="name" type="text" placeholder="Nombre" required />
            <label for="name" id="msj_name" class="mensaje"></label>
            <br/>
            <label class="label_caja" for="lastname">Y tu apellido...</label>
            <input class="caja" type="text" id="lastname" name="lastname" placeholder="Apellido" required/>
            <label for="lastname" id="msj_lastname" class="mensaje"></label>
            <br/>
            <label class="label_caja" for="email">¿Tu correo electrónico?</label>
            <input class="caja" type="email" placeholder="tucorreo@ejemplo.com" id="email" name="email" required  />
            <label for="email" id="msj_email" class="mensaje"></label>
            <br/>
            <div class="label_caja">Dinos por favor tu género:</div>
            <div id="contenedor_radios" name="sex">
                <input class="radio" type="radio" name="sex" id="radio_male" value="male">
                <label id="boton_hombre" class="label_radio" for="radio_male">Hombre</label>
                <input class="radio" type="radio" name="sex" id="radio_female" value="female">
                <label id="boton_mujer" class="label_radio" for="radio_female">Mujer</label>
            </div>
            <div id="msj_gender" class="mensaje"></div>
            <br/>
            <input class="caja" type="submit" id="enviar" value="enviata">
        </fieldset>
    </form>
</div>
@stop