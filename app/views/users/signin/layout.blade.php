@extends('layout')

@section('title')
Formulario de registro
@endsection

@section('resources')
{{HTML::script("javascript/jquery-1.8.0.min.js")}}
{{HTML::script("javascript/modeloJquery.js")}}
{{HTML::script("javascript/controladorJQuery.js")}}
{{HTML::style("css/estilos_principal.css")}}
{{HTML::style("css/registro.css")}}
@endsection

@section('body')
@yield('header_panel')
<div id="contenedor_cuerpo">
    <div id="contenedor_registro">
        <h1> @yield('header')</h1>
        <form id="formulario" action="@yield('action')" method="POST">
            <fieldset>
                @yield('inputs')

            </fieldset>
        </form>
    </div>
</div>
@yield('footer_principal')
@endsection
