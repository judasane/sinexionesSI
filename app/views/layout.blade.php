<!DOCTYPE html>
<html>
    <head>
        @section('head')
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        @show     
    </head>
    <body>
        <!--Header estático-->
        <header id="header_principal">
            @section('header_principal')
                @include('header')
            @show
        </header>
        <!--Contenedor principal-->
        <div id="contenedor_principal">
            @yield('contenedor_principal')
            <!--Cuerpo de la página-->
            <div id="contenedor_cuerpo">
                @yield('contenedor_cuerpo')
            </div>
        </div>
        <!--Footer Principal-->
        <footer id="footer_principal">
            @yield('footer_principal')
        </footer>
    </body>
</html>
