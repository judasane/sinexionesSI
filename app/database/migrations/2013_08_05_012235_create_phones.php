<?php   

use Illuminate\Database\Migrations\Migration;

class CreatePhones extends Migration {

    /**
     * Crea una tabla en la que se alojarán los teléfonos tanto de usuarios,
     * como de otro tipo de entidad que use teléfonos.
     *
     * @return void
     */
    public function up() {
        Schema::create('phones', function($table) {
                    $table->engine = 'InnoDB';
                    $table->increments('id');
                    $table->integer("owner_id");
                    $table->string("owner_type");
                    $table->string('number', '20');
                    $table->enum('type', array('cellphone', 'landline'));
                    $table->timestamps();
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop("phones");
    }

}
