<?php

use Illuminate\Database\Migrations\Migration;

class CreateRoles extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('roles', function($table) {
                    $table->engine = 'InnoDB';
                    $table->increments('id');
                    $table->string('name',20);
                    $table->string('description');
                    $table->timestamps();
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop("roles");
    }

}
