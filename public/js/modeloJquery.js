function procederBlur() {
    var mensaje = "";
    actualizarMensaje(this, mensaje);
    mensaje = validarCadena(this);
}
function procederSubmit() {
    var seleccionados = $(".radio:checked");
    if (seleccionados.length < 1) {
        actualizarMensaje($("#contenedor_radios"),"<img src='../img/error.png'/>Debes seleccionar alguno");
        return false;
    }
}

function validarToggleButton(){
    actualizarMensaje($("#contenedor_radios"),"<img src='../img/ok.png'/>Perfecto!");
}

//Funciones de validación

//Valida el campo usando ajax
function validarCadena(elemento) {

    //parte de ajax
    var name = elemento.getAttribute("name");

    var cadena = elemento.value;

    var variablesUrl = name + "=" + cadena;

    if (this.temporizador) {
        clearTimeout(this.temporizador);
    }
    actualizarMensaje(elemento, '<img src="../img/cargando.gif" alt="cargando" height="16" width="16"/>Validando..');
    this.temporizador = setTimeout(function() {
        $.get("signin-validation", variablesUrl, function(datos) {
            var mensaje = "";
            if (datos == 1) {
                mensaje = "<img src='../img/ok.png'/>Perfecto!";
            } else {
                mensaje = "<img src='../img/error.png'/>Debes ingresar un valor válido";

            }
            actualizarMensaje(elemento, mensaje);
        });
    });
    return mensaje;
}

function actualizarMensaje(elemento, mensaje) {
    $(elemento).next().html(mensaje);
}
