<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="css/pendientes.css"/>
        
    </head>
    <body>
        <h1>Cosas pendientes por hacer</h1>
        <div id="contenedor">
            <ol>
                <li>Crear vistas</li>
                <ul>
                    <li>Crear htmls</li>
                    <ul>
                        <li>postSignin</li>
                        <li>getFinishSignin</li>
                        
                    </ul>
                    <li>Organizar el orden de plantillas</li>
                    <li>Crear cabecera y pié de página común de todas las páginas</li>
                    <li>Diseño:</li>
                    <ul>
                        <li>Hojas de estilos</li>
                        <li>Diseñar logo</li>
                    </ul>
                </ul>
                <li>Definir sistema de accesos</li>
                <ul>
                    <li>Manejo de autenticación</li>
                    <ul>
                        <li>Registro</li>
                        <ul>
                            <li>Rediseño de formularios</li>
                            <li>Validación de formularios por ajax</li>
                            <li>Asignar rol a usuarios nuevos</li>
                        </ul>
                        <li>Recordatorios de contraseñas</li>
                        <li>Darse de baja</li>
                    </ul>
                    <li>Manejo de filtros</li>
                    <ul>
                        <li>login selectivo</li>
                        <ul>
                            <li>Clientes</li>
                        </ul>
                    </ul>
                    <li>Diseño de arbol de carpetas</li>
                </ul>
            </ol>
        </div>

    </body>
</html>




