<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Regístrate en Sinexiones</title>
        <link media="all" type="text/css" rel="stylesheet" href="http://localhost/sinexiones/public/header_principal.css">
        <link media="all" type="text/css" rel="stylesheet" href="http://localhost/sinexiones/public/layout.css">
        <link media="all" type="text/css" rel="stylesheet" href="http://localhost/sinexiones/public/registro.css">

    </head>
    <body>
        <!--Header estático-->
        <header id="header_principal">
            <nav>
                <ul>
                    <li><a href="">Inicio</a></li>
                    <li><a href="">Nosotros</a></li>
                    <li><a href="">Servicios</a></li>
                    <li><a href="">Contacto</a></li>
                </ul>
            </nav>            
            <div id="logo">
                <img src="img/logo1000.png" height="70" width="70" alt="logo">sinexiones
            </div>
        </header>
        <!--Contenedor principal-->
        <div id="contenedor_principal">
            <!--Cuerpo de la página-->
            <div id="contenedor_cuerpo">
                <div id="contenedor_registro">
                    <h1>Formulario de Registro</h1>
                    <form action="funcion" method="POST" id="form_registro">
                        <fieldset form="form_registro">
                            <legend>Ingresa tus datos para formar parte de Sinexiones</legend>
                            <label class="label_caja" for="name">Ingresa tu nombre:</label>
                            <input class="caja" id="name" name="name" type="text" placeholder="Nombre" required autofocus/>
                            <label for="name" id="msj_name" class="mensaje"></label>
                            <br/>
                            <label class="label_caja" for="lastname">Y tu apellido...</label>
                            <input class="caja" type="text" id="lastname" name="lastname" placeholder="Apellido" required/>
                            <label for="lastname" id="msj_lastname" class="mensaje"></label>
                            <br/>
                            <label class="label_caja" for="email">¿Tu correo electrónico?</label>
                            <input class="caja" type="email" placeholder="tucorreo@ejemplo.com" id="email" name="email" required  />
                            <label for="email" id="msj_email" class="mensaje"></label>
                            <br/>
                            <div id="contenedor_radios">
                                <input class="radio" type="radio" name="sex" id="radio_male" value="male" required>
                                <label id="boton_hombre" class="label_radio" for="radio_male">Hombre</label>
                                <input class="radio" type="radio" name="sex" id="radio_female" value="female">
                                <label id="boton_mujer" class="label_radio" for="radio_female">Mujer</label>
                            </div>
                            <label id="msj_gender" class="mensaje"></label>
                            <br/>
                            <input class="caja" type="submit" id="enviar">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!--Footer Principal-->
        <footer id="footer_principal">
            
        </footer>
    </body>
</html>